module chainmaker.org/chainmaker/consensus-tbft/v2

go 1.15

require (
	chainmaker.org/chainmaker/chainconf/v2 v2.2.2
	chainmaker.org/chainmaker/common/v2 v2.2.1
	chainmaker.org/chainmaker/consensus-utils/v2 v2.2.1
	chainmaker.org/chainmaker/localconf/v2 v2.2.1
	chainmaker.org/chainmaker/logger/v2 v2.2.1
	chainmaker.org/chainmaker/lws v1.0.0
	chainmaker.org/chainmaker/pb-go/v2 v2.2.1
	chainmaker.org/chainmaker/protocol/v2 v2.2.2
	chainmaker.org/chainmaker/utils/v2 v2.2.2
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.19.1
)
