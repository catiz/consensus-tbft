set -x
VERSION221=v2.2.1
VERSION222=v2.2.2
go get chainmaker.org/chainmaker/chainconf/v2@${VERSION222}
go get chainmaker.org/chainmaker/common/v2@${VERSION221}
go get chainmaker.org/chainmaker/consensus-utils/v2@${VERSION221}
go get chainmaker.org/chainmaker/localconf/v2@${VERSION221}
go get chainmaker.org/chainmaker/logger/v2@${VERSION221}
go get chainmaker.org/chainmaker/pb-go/v2@${VERSION221}
go get chainmaker.org/chainmaker/protocol/v2@${VERSION222}
go get chainmaker.org/chainmaker/utils/v2@${VERSION222}
go get chainmaker.org/chainmaker/lws@1.0.0
go mod tidy
go build ./...
